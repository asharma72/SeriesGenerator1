﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCSeriesGenerator.Models;
namespace MVCSeriesGenerator.Controllers
{
    public class DisplaySeriesController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Index(string txtRange)
        {
            //Tested by Ajay Gitlab first commit
            SeriesManager ser = new SeriesManager();
            List<Series> finalSeries = ser.GenerateSeries(Convert.ToInt32(txtRange));
            return Json(finalSeries);
        }

    }
}
