﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCSeriesGenerator.Models
{
    public class Series
    {
        public Series() { }
        public Series(int idx, int num, bool exp)
        {
            this.Idx = idx;
            this.Num = num;
            this.Exp = exp;
        }
        public int Idx { get; set; }
        public int Num { get; set; }
        public bool Exp { get; set; }
    }
}