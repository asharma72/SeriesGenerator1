﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCSeriesGenerator.Models
{
    public class SeriesManager
    {
        public SeriesManager() { }
        public List<Series> GenerateSeries(int maxRange)
        {
            List<Series> initialSeries = new List<Series>();
            Series ser1 = new Series(1, 1, false);
            Series ser2 = new Series(2, 1, false);
            Series ser3 = new Series(3, 1, false);
            initialSeries.Add(ser1);
            initialSeries.Add(ser2);
            initialSeries.Add(ser3);

            int localRange = 1;
                       
            //1,1,1,3,5,9,17,31,57,105
            while (localRange < maxRange)
            {
                Series nextSeries = new Series();
                nextSeries.Num = 0;
                bool setExpire = false;
                int counter = 0;
                foreach (Series s in initialSeries)
                {
                    if (!s.Exp && !setExpire)
                    {
                        s.Exp = true;
                        setExpire = true;
                        counter++;

                        nextSeries.Num += s.Num;
                        nextSeries.Idx = initialSeries.Count + 1;
                        nextSeries.Exp = false;
                    }
                    if (counter == 3)
                        break;
                    if (!s.Exp)
                    {
                        counter++;
                        nextSeries.Num += s.Num;
                    }

                }
                if (nextSeries.Num > maxRange)
                    break;
                initialSeries.Add(nextSeries);
                Series lastSeries = initialSeries.LastOrDefault();
                localRange = lastSeries.Num;
            }
            return initialSeries;
        }
    }
}