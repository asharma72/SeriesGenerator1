﻿$(function () {
    $('#btnSubmit').unbind('click');
    $('#btnSubmit').on('click', function () {
        requireValidation($("#txtRange"));
    });
});
function requireValidation(ctrl) {
    //var txtRange = $("#txtRange");
    var txtRange = ctrl;
    var txtRangeVal = txtRange.val();
    var intRangeVal = parseInt(txtRangeVal);
    if (txtRangeVal === "") {
        alert("Please enter number");
    } else if(intRangeVal > 999999999){
        alert("Please enter valid number between 1 - 999999999");
    } else {
        getSeries();
    }
}
function getSeries() {
    $.ajax({
        url: "/DisplaySeries",
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify({
            txtRange: $("#txtRange").val()
        }),
        async: true,
        success: function (series) {
            //console.log(series);
            processSeries(series);
        }
    });
}
function processSeries(series) {
    var displaySeries = "<div>Series : </div>";
    var lengthSeries = series.length;
    series.forEach(function (ser, idx) {
        if (idx % 2 == 0)
            displaySeries += "<div id='divSeries' style='padding:10px;border-style:solid;border-width:1px;border-collapse: collapse;background-color:red;color:white;float:left;width:" + ser.Num.length * 4 + "px'>" + ser.Num + "</div>";
        else
            displaySeries += "<div id='divSeries' style='padding:10px;border-style:solid;border-width:1px;border-collapse: collapse;background-color:green;color:white;float:left;width:" + ser.Num.length * 4 + "px'>" + ser.Num + "</div>";

    });
    $("div[id=divShowNumber]").empty();

    var searchDiv = divForSearch();

    var divContainer = $("<div>");
    divContainer.css({
        'padding': '10px',
        'height': '100px'
    });
    divContainer.append(displaySeries);
    //divContainer.append(searchDiv);

    //$("div[id=divShowNumber]").append(displaySeries);
    //$("div[id=divShowNumber]").append(searchDiv);
    $("div[id=divShowNumber]").append(divContainer);
    $("div[id=divShowNumber]").append(searchDiv);

    $('#btnFind').on('click', function () {
        getAndDisplayResult();
    });
}

function divForSearch() {
    var div = $("<div>");
    div.css({
        'float': 'left',
        'padding': '10px',

    });
    var txtNum = $("<input>").attr({
        'type': 'text',
        'id': 'txtSearch',
        'placeholder': 'Enter Number'

    });
    var txtIndex = $("<input>").attr({
        'type': 'text',
        'id': 'txtIndex',
        'placeholder': 'Enter Index'
    });
    var btnFind = $("<input>").attr({
        'type': 'submit',
        'id': 'btnFind',
        'value': 'Find'
    });

    var lblResult = $("<label>").attr({
        'for': 'lblResult',
    });

    txtNum.css({
        'padding': '10px',
        'margin-right': '10px',
        //'width': '20px'
    });
    txtIndex.css({
        'padding': '10px',
        'margin-right': '10px',
        //'width': '20px'
    });
    btnFind.css({
        'padding': '10px',
        'margin-right': '10px',
        //'width': '20px'
    });
    div.append(txtNum).append(txtIndex).append(btnFind).append(lblResult);



    return div;
}
function getAndDisplayResult() {
    var allSeriesDiv = $("div[id=divSeries]");
    var num = $("#txtSearch").val();
    var idx = $("#txtIndex").val();
    var allDivisibleData = [];
    allSeriesDiv.each(function () {
        var seriesDiv = $(this);
        var seriesData = parseInt(seriesDiv.text());
        if (seriesData % num === 0)
            allDivisibleData.push(seriesData);
    });

    var lblResult = $("label[for=lblResult]");
    lblResult.empty();
    if (allDivisibleData.length < idx) {
        lblResult.text("Entered Index is out of range");
    }
    else {
        lblResult.text(allDivisibleData[idx - 1]);
    }
}