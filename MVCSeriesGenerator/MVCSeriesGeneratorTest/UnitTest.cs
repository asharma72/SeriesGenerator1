﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MVCSeriesGenerator.Models;
using System.Collections.Generic;


namespace MVCSeriesGeneratorTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void SeriesManagerTestMethod()
        {
            SeriesManager ser = new SeriesManager();
            List<Series> finalSeries = ser.GenerateSeries(Convert.ToInt32(100));
            Assert.IsNotNull(finalSeries);
        }
    }
}
